package main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JsoupMain {

	public static void main(String[] args) {
		
		try {
			
			//Document doc = Jsoup.connect("https://www.hepsiburada.com/").get();
			
			HashMap<String, SW_Character> hmHeroes = new HashMap<>();
			
			File fHeroes = new File("D:\\Users\\alphank\\Desktop\\SWGOH\\Star Wars Galaxy of Heroes Database � SWGOH.GG.html");
			Document dHeroes = Jsoup.parse(fHeroes, "UTF-8", "http://example.com/");
			
			
			Elements eHeroes = dHeroes.select("body > div.container.p-t-md > div.content-container > "
					+ "div.content-container-primary.character-list.homepage-list.char-search > "
					+ "ul > li > a > div.media-heading > h5");
			
			int sw_char_id = 0;
			
			for (Element element : eHeroes) {
				SW_Character sw_char = new SW_Character();
				sw_char.id = sw_char_id++;
				sw_char.p1_count = 0;
				sw_char.p2_count = 0;
				sw_char.p3_count = 0;
				sw_char.p4_count = 0;
				sw_char.name = element.text();
				
				hmHeroes.put(element.text(), sw_char);
				System.out.println(element.text());
			}


			
			
			HashMap<String, String> hmPlayers = new HashMap<>();

			File fPlayers = new File("D:\\Users\\alphank\\Desktop\\SWGOH\\808MSK808 � SWGOH.GG.html");
			Document dPlayers = Jsoup.parse(fPlayers, "UTF-8", "http://example.com/");
			
			Elements ePlayers = dPlayers.select("body > div.container.p-t-md > div.content-container > "
					+ "div.content-container-primary.character-list > ul > li.media.list-group-item.p-0.b-t-0 > "
					+ "div > table > tbody > tr > td.footable-first-visible > a");
			
			for (Element element : ePlayers) {
				hmPlayers.put(element.text(), element.attr("href"));

				System.out.println(element.text() + " : " + element.attr("href"));
			}
			
			
			
			File fRoster = new File("D:\\Users\\alphank\\Desktop\\SWGOH\\TorridZone17's Characters on Star Wars Galaxy of Heroes � SWGOH.GG.html");
			Document dRoster = Jsoup.parse(fRoster, "UTF-8", "http://example.com/");
			
			Elements eRoster = dRoster.select("body > div.container.p-t-md > div.content-container > "
					+ "div.content-container-primary.character-list > ul > "
					+ "li.media.list-group-item.p-a.collection-char-list > div > div");
			
			for (Element element : eRoster) {
				String charName = element.select("div > div.collection-char-name > a").get(0).text();
				String power = element.select("div > div.collection-char-gp > "
						+ "div.collection-char-gp-label > span.collection-char-gp-label-value").get(0).text();
				
				
				Element eStar6 = element.select("div > div.player-char-portrait > a > div.star.star6.star-inactive").first();
				Element eStar7 = element.select("div > div.player-char-portrait > a > div.star.star7.star-inactive").first();
				
				String star = (eStar6 != null) ? "0" : (eStar7 != null) ? "6" : "7";
				
				System.out.println(charName + ", " + power + ", " + star);
			}
			
			
			
			
			
			String sPhase1 = new String(Files.readAllBytes(Paths.get("D:\\Users\\alphank\\Desktop\\SWGOH\\phase1.txt")));
			
			String[] sPhase1Chars = sPhase1.split("\t");
			
			System.out.println(sPhase1Chars.length);
			
			for (String sPhase1Char : sPhase1Chars) {

				if (hmHeroes.containsKey(sPhase1Char) == false) {
					System.out.println("Character Name Error = " + sPhase1Char);
				}
				else {
					SW_Character sw_char = hmHeroes.get(sPhase1Char);
					
					sw_char.p1_count++;
				}
			}
			
			
			System.out.println("******************************************");
			System.out.println("Phase 1 Platoon Requirements");
			for (SW_Character sw_char : hmHeroes.values()) {
				if (sw_char.p1_count != 0) {
					System.out.println(sw_char.name + " " + sw_char.p1_count);
				}
			}
			

			
			String sPhase2 = new String(Files.readAllBytes(Paths.get("D:\\Users\\alphank\\Desktop\\SWGOH\\phase2.txt")));
			
			String[] sPhase2Chars = sPhase2.split("\t");
			
			System.out.println(sPhase2Chars.length);
			
			for (String sPhase2Char : sPhase2Chars) {

				if (hmHeroes.containsKey(sPhase2Char) == false) {
					System.out.println("Character Name Error = " + sPhase2Char);
				}
				else {
					SW_Character sw_char = hmHeroes.get(sPhase2Char);
					
					sw_char.p2_count++;
				}
			}
			
			
			System.out.println("******************************************");
			System.out.println("Phase 2 Platoon Requirements");
			for (SW_Character sw_char : hmHeroes.values()) {
				if (sw_char.p2_count != 0) {
					System.out.println(sw_char.name + " " + sw_char.p2_count);
				}
			}
			

		
			String sPhase3 = new String(Files.readAllBytes(Paths.get("D:\\Users\\alphank\\Desktop\\SWGOH\\phase3.txt")));
			
			String[] sPhase3Chars = sPhase3.split("\t");
			
			System.out.println(sPhase3Chars.length);
			
			for (String sPhase3Char : sPhase3Chars) {

				if (hmHeroes.containsKey(sPhase3Char) == false) {
					System.out.println("Character Name Error = " + sPhase3Char);
				}
				else {
					SW_Character sw_char = hmHeroes.get(sPhase3Char);
					
					sw_char.p3_count++;
				}
			}
			
			
			System.out.println("******************************************");
			System.out.println("Phase 3 Platoon Requirements");
			for (SW_Character sw_char : hmHeroes.values()) {
				if (sw_char.p3_count != 0) {
					System.out.println(sw_char.name + " " + sw_char.p3_count);
				}
			}

		
		
			String sPhase4 = new String(Files.readAllBytes(Paths.get("D:\\Users\\alphank\\Desktop\\SWGOH\\phase4.txt")));
			
			String[] sPhase4Chars = sPhase4.split("\t");
			
			System.out.println(sPhase4Chars.length);
			
			for (String sPhase4Char : sPhase4Chars) {

				if (hmHeroes.containsKey(sPhase4Char) == false) {
					System.out.println("Character Name Error = " + sPhase4Char);
				}
				else {
					SW_Character sw_char = hmHeroes.get(sPhase4Char);
					
					sw_char.p4_count++;
				}
			}
			
			
			System.out.println("******************************************");
			System.out.println("Phase 4 Platoon Requirements");
			for (SW_Character sw_char : hmHeroes.values()) {
				if (sw_char.p4_count != 0) {
					System.out.println(sw_char.name + " " + sw_char.p4_count);
				}
			}

		
		
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
